# SeqUUID

Simple sequential UUID.v1, great for distributed systems writing to
databases.

## Examples

Time based uuid: `58e0a7d7-eebc-11d8-9669-0800200c9a66`  
Sequential UUID: `11d8 eebc 58e0a7d7 9669 0800200c9a66`

# API

- `encode` Transform a time-based UUIDv1 to an ordered UUID
- `decode` Transform an ordered UUID v1 to a regular time-based UUIDv1
- `generate` generate a new ordere UUID
- `validate` Validate an ordered UUID
- `assert` Validate an ordered UUID using assert

# License

ISC
