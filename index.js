'use strict';

const uuid = require('uuid');
const R = require('ramda');
const Joi = require('joi');

const parseNodeId = R.pipe(
    R.splitEvery(2),
    R.map(x => parseInt(x, 16)));

// Node ID, used to create a new UUID
// the node id represents the machine
const node = parseNodeId(process.env.NODE_ID || '00010E0E0400');

const isString = R.is(String);

/**
 * Transform a time-based UUIDv1 to an ordered UUID
 *
 * 58e0a7d7-eebc-11d8-9669-0800200c9a66 => 11d8 eebc 58e0a7d7 9669 0800200c9a66
 * (spaces added for readability)
 *
 * @param {UUID.v1} uuid
 * @return {SeqUUID}
 */
exports.encode =
    // split string
    R.pipe(R.split('-'),
           R.of,
           // apply a list of functions to the resulting array
           // reordering the UUID parts
           R.ap([R.nth(2),
                 R.nth(1),
                 R.nth(0),
                 R.nth(3),
                 R.nth(4),
                ]),
           // join the resulting list
           R.join(''));

/**
 * Transform an ordered UUID v1 to a regular time-based UUIDv1
 *
 * 11d8 eebc 58e0a7d7 9669 0800200c9a66 => 58e0a7d7-eebc-11d8-9669-0800200c9a66
 * (spaces added for readability)
 *
 * @param {SeqUUID} uuid
 * @return {UUID.v1}
 */
exports.decode =
    R.pipe(
        // wrap the string inside an array
        R.of,
        // Apply a list of functions to the resulting Array
        // reordering as by UUIDv1 spec
        R.ap([
            R.slice(8, 16),
            R.slice(4, 8),
            R.slice(0, 4),
            R.slice(16, 20),
            R.slice(20, 32),
        ]),
        // Join the resulting list
        R.join('-'));

/**
 * Generate a new ordered UUID
 *
 * @return {SeqUUID} uuid
 */
exports.generate = function generate() {
    const id = uuid.v1({ node });

    return exports.encode(id);
};

const testUUID = R.test(
        /^[0-9a-f]{8}-[0-9a-f]{4}-1[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);

/**
 * Validate ordered UUIDs
 *
 * @param {String} str string representing a SeqUUID
 * @return {Bool}
 */
exports.validate = function validate(str) {
    let s = str;

    // buffer
    if (!isString(s)) {
        if (Buffer.isBuffer(s)) {
            s = s.toString();
        } else {
            return false;
        }
    }

    if (R.length(s) !== 32) return false;

    // check if the version is 1
    if ((s.charAt(0) | 0) === 1) return false;

    const decoded = exports.decode(s);

    return testUUID(decoded);
};

/**
 * Assert that the passed ID is a SequentialUUID
 *
 * @param {String} str string representing a sequantial str
 * @return {Void}
 * @throws AssertionError
 */
exports.assert = function assert(str) {
    return console.assert(validate(str),
                          'Id must be a valid Sequential UUID');
}

exports.schema = Joi
    .string()
    .guid();

